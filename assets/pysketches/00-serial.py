import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import serial
import signal
import time
from collections import deque

com_port = 'COM7'
state = 0
MAX_ITEMS = 200

def exit_gracefully(signum, frame):
    print('Bye.')
    ser.baudrate = 1200
    ser.close()
    exit()

fig = plt.figure()
ax = plt.axes(xlim=(0, 200), ylim=(-1024, 1024))
line, = ax.plot([], [], lw=2)
d = deque([0] * MAX_ITEMS, maxlen=MAX_ITEMS)

def init():
    line.set_data([], [])
    return line,

def animate(i, ser):
    s = ser.readline()
    k = s.strip().split()
    d.append(k[0])
    x = np.arange(MAX_ITEMS)
    y = np.gradient(np.array(list(d), dtype=int))
    line.set_data(x, y)
    return line,

# plt.axis([0, 10, 0, 2048])
# plt.ion()

with serial.Serial(com_port, 9600) as ser:
    ser.write(b'initialize-nodejs-arduino\n')
    ser.flush()
    signal.signal(signal.SIGINT, exit_gracefully)
    while True:
        s = ser.readline()
        if b'acknowledged-nodejs-arduino' in s and state is 0:
            state = 1
            print('in state 1')
        elif b'nismed-earthquake-sensing' in s and state is 1:
            state = 2
            print('in state 2')
            ser.write(b'start\n')
            ser.flush()
        else:
            # k = s.strip().split()
            # print('Received {} {} {}'.format(int(k[0]), int(k[1]), int(k[2])))
            anim = animation.FuncAnimation(fig, animate, init_func=init, frames=200, interval=10, blit=True, fargs=(ser,))
            plt.show()
            break
            # ser.close()
            # exit()