'use strict';

let interprocessListeners = function (isDebug, settings, ipc, state, mainWindow) {
  ipc.on('initReady', (event) => {
    state.emit('startScript');
  });
}

module.exports = interprocessListeners;