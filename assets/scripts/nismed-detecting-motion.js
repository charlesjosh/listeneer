'use strict';

const electron = require('electron');
const remote = electron.remote;
const dialog = remote.dialog;
const fs = require('fs');
const path = require('path');
const canvasBuffer = require('electron-canvas-to-buffer');

const ipc = electron.ipcRenderer;

const valueHolder = document.getElementsByClassName('motion-value')[0];
const recordButton = document.getElementsByClassName('motion-record')[0];
const graphsButton = document.getElementsByClassName('motion-graphs')[0];
const valuesButton = document.getElementsByClassName('motion-values')[0];

let distancesCanvas = document.getElementById('distances-chart');
let velocitiesCanvas = document.getElementById('velocities-chart');
let accelerationCanvas = document.getElementById('acceleration-chart');
let overlapCanvas = document.getElementById('overlap-chart');
let distancesCtx = distancesCanvas.getContext('2d');
let velocitiesCtx = velocitiesCanvas.getContext('2d');
let accelerationCtx = accelerationCanvas.getContext('2d');
let overlapCtx = overlapCanvas.getContext('2d');

let distancesData = {
    datasets: [
        {
            label: 'Distance',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(255,99,132,1)',
            backgroundColor: 'rgba(255,99,132,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let velocitiesData = {
    datasets: [
        {
            label: 'Velocity',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(255,196,13,1)',
            backgroundColor: 'rgba(255,196,13,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let accelerationData = {
    datasets: [
        {
            label: 'Acceleration',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(0,171,169,1)',
            backgroundColor: 'rgba(0,171,169,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let overlapData = {
    datasets: [
        {
            label: 'Distance',
            borderWidth: 2,
            borderColor: 'rgba(255,99,132,1)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        },
        {
            label: 'Velocity',
            borderWidth: 2,
            borderColor: 'rgba(255,196,13,1)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        },
        {
            label: 'Acceleration',
            borderWidth: 2,
            borderColor: 'rgba(0,171,169,1)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let chartOptions = {
    animationSteps: 15,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
            type: 'linear',
            position: 'bottom'
        }],
        yAxes: [{
            scaleLabel: {
                display: true,
                labelString: 'wow'
            }
        }]
    },
    legend: {
        display: false
    }
}

let distancesOptions = JSON.parse(JSON.stringify(chartOptions));
let velocitiesOptions = JSON.parse(JSON.stringify(chartOptions));
let accelerationOptions = JSON.parse(JSON.stringify(chartOptions));
let overlapOptions = JSON.parse(JSON.stringify(chartOptions));

distancesOptions.scales.yAxes[0].scaleLabel.labelString = 'Distance (mm)';
velocitiesOptions.scales.yAxes[0].scaleLabel.labelString = 'Velocity (mm/s)';
accelerationOptions.scales.yAxes[0].scaleLabel.labelString = 'Acceleration (mm/s²)';
overlapOptions.scales.yAxes[0].scaleLabel.labelString = 'Overlap (R: Dis, Y: Vel, B: Acc)';

let distancesChart = new Chart.Line(distancesCtx, {data: distancesData, options: distancesOptions});
let velocitiesChart = new Chart.Line(velocitiesCtx, {data: velocitiesData, options: velocitiesOptions});
let accelerationChart = new Chart.Line(accelerationCtx, {data: accelerationData, options: accelerationOptions});
let overlapChart = new Chart.Line(overlapCtx, {data: overlapData, options: overlapOptions});

let isRecording = false;
let initialTime = 0;

ipc.on('motion-data', (event, value) => {
//   valueHolder.innerHTML = value;
    let dataArray = value.split(' ');
    if (initialTime === 0) initialTime = dataArray[0];

    if (isRecording) {
        distancesData.datasets[0].data.push({x: (dataArray[0] - initialTime) / 1000, y: dataArray[1]});
        velocitiesData.datasets[0].data.push({x: (dataArray[2] - initialTime) / 1000, y: dataArray[3]});
        accelerationData.datasets[0].data.push({x: (dataArray[4] - initialTime) / 1000, y: dataArray[5]});

        overlapData.datasets[0].data.push({x: (dataArray[0] - initialTime) / 1000, y: dataArray[1]});
        overlapData.datasets[1].data.push({x: (dataArray[2] - initialTime) / 1000, y: dataArray[3]});
        overlapData.datasets[2].data.push({x: (dataArray[4] - initialTime) / 1000, y: dataArray[5]});

        distancesChart.update();
        velocitiesChart.update();
        accelerationChart.update();
        overlapChart.update();
    }
});

graphsButton.addEventListener('click', (event) => {
    let dis = canvasBuffer(distancesCanvas, 'image/png');
    let vel = canvasBuffer(velocitiesCanvas, 'image/png');
    let acc = canvasBuffer(accelerationCanvas, 'image/png');
    let ove = canvasBuffer(overlapCanvas, 'image/png');

    dialog.showOpenDialog({properties: ['openDirectory', 'createDirectory']}, (filePaths) => {
        if (filePaths === undefined) return;

        fs.writeFile(path.join(filePaths[0], 'distance.png'), dis, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'velocity.png'), vel, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'acceleration.png'), acc, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'overlap.png'), ove, (err) => { err ? console.log(err) : null });
    });
});

valuesButton.addEventListener('click', (event) => {
    let dis = overlapData.datasets[0].data;
    let vel = overlapData.datasets[1].data;
    let acc = overlapData.datasets[2].data;
    let toCSV = '';
    toCSV += 'time1,distance-cm,time2,velocity-cmps,time3,acceleration-cmpss\n';
    overlapData.datasets[0].data.map((k, i) => {
        toCSV += `${k.x}, ${k.y}, ${vel[i] ? vel[i].x : ''}, ${vel[i] ? vel[i].y : ''}, ${acc[i] ? acc[i].x : ''}, ${acc[i] ? acc[i].y : ''}\n`;
    });

    dialog.showSaveDialog({defaultPath: 'detectingMotion.csv'}, (fileName) => {
        if (fileName === undefined) return;
        
        fs.writeFile(fileName, toCSV, (err) => { err ? console.log(err) : null });
    });
});

recordButton.addEventListener('click', (event) => {
    if (isRecording) {
        recordButton.classList.remove('stop');
        recordButton.classList.add('start');
        recordButton.innerHTML = '<i data-motion="record" class="fa fa-play fa-fw" aria-hidden="true"></i> Start Recording';
        
        graphsButton.classList.remove('hide');
        valuesButton.classList.remove('hide');
    } else {
        recordButton.classList.remove('start');
        recordButton.classList.add('stop');
        recordButton.innerHTML = '<i data-motion="record" class="fa fa-stop fa-fw" aria-hidden="true"></i> Stop Recording';
        
        graphsButton.classList.add('hide');
        valuesButton.classList.add('hide');

        distancesData.datasets[0].data = [];
        velocitiesData.datasets[0].data = [];
        accelerationData.datasets[0].data = [];

        overlapData.datasets[0].data = [];
        overlapData.datasets[1].data = [];
        overlapData.datasets[2].data = [];
        
        distancesChart.update();
        velocitiesChart.update();
        accelerationChart.update();
        overlapChart.update();
        
        initialTime = 0;
    }
    isRecording = ~isRecording;
});