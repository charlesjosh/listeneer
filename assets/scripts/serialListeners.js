'use strict';

const SerialPort = require("serialport");
let Serial;

let listPortTimer = null;

let serialListeners = function (isDebug, settings, ipc, state, mainWindow) {
  // Initializes the whole program.
  state.on('startScript', () => {
    state.emit('listAllPorts');
  });

  // This forces the Arduino Leonardo to restart on exit.
  state.on('exitScript', (callback) => {
    // The change in speed is harmless for other boards (tested a simulation sketch with an Arduino Mega).
    if (Serial && Serial.isOpen()) {
      Serial.update({ baudRate: 1200 }, (err) => {
        if (err) {
          isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' failed to update speed upon exit. ' + err) : '';
        } else {
          isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' changed speed upon exit.') : '';
        }
        Serial.close((err) => {
          if (err) {
            isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' failed to close upon exit. ' + err) : '';
          } else {
            isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' properly closed upon exit.') : '';
            callback();
          }
        });
      });
    } else {
      callback();
    }
  })

  // This one requests a listing of all open Serial Communication ports, and filters those tagged 'Arduino' on the Manufacturer information.
  state.on('listAllPorts', () => {
    Serial && Serial.isOpen() ? Serial.close() : '';
    SerialPort.list((err, ports) => {
      settings.probablePorts = ports.map(port => port.manufacturer.toLowerCase().indexOf('arduino') === -1 ? false : port.comName);

      if (settings.probablePorts.length === 0) {
        isDebug ? console.log('No Arduinos found. Checking again in a second.') : '';
        listPortTimer && typeof listPortTimer.clearTimeout === 'function' ? listPortTimer.clearTimeout() : '';
        listPortTimer = setTimeout(() => { state.emit('listAllPorts') }, settings.searchTimeout);
      } else {
        isDebug ? console.log('Arduinos found: ' + settings.probablePorts) : '';
        settings.portIndex = -1;
        state.emit('openAPort');
      }

    });
  });

  // Checks if Arduino responds correctly.
  state.on('openAPort', () => {
    settings.portIndex++;
    if (settings.portIndex >= settings.probablePorts.length) {
      isDebug ? console.log('No Arduinos responded correctly. Restarting search.') : '';
      listPortTimer && typeof listPortTimer.clearTimeout === 'function' ? listPortTimer.clearTimeout() : '';
      listPortTimer = setTimeout(() => { state.emit('listAllPorts') }, settings.searchTimeout);
    } else {
      Serial && Serial.isOpen() ? Serial.close() : '';
      Serial = new SerialPort(settings.probablePorts[settings.portIndex], { parser: SerialPort.parsers.readline('\n') }, (err) => {
        if (err) {
          isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' failed. ' + err) : '';
          Serial && Serial.isOpen() ? Serial.close() : '';
          return state.emit('openAPort');
        } else {
          return setTimeout(() => { state.emit('handshakeToPort') }, settings.searchTimeout * 2);
        }
      });

      Serial.on('data', (data) => {
        isDebug ? console.log('Data: ' + data) : '';
        mainWindow.isDestroyed() ? '' : state.emit('serialResponse', data);
      });

      Serial.on('error', (err) => {
        console.log('Error: ', err.message);
        if (err.message.indexOf('Port is not open') != -1) {
          Serial = new SerialPort(settings.probablePorts[settings.portIndex], { parser: SerialPort.parsers.readline('\n') }, (err) => {
            console.log(err);
            listPortTimer && typeof listPortTimer.clearTimeout === 'function' ? listPortTimer.clearTimeout() : '';
            listPortTimer = setTimeout(() => { state.emit('listAllPorts') }, settings.searchTimeout);
          });
        }
      });

      Serial.on('disconnect', (err) => {
        isDebug ? console.log('Arduino currently disconnected.') : '';
        mainWindow.isDestroyed() ? '' : mainWindow.webContents.send('arduino-state', 'disconnected');
        listPortTimer && typeof listPortTimer.clearTimeout === 'function' ? listPortTimer.clearTimeout() : '';
        listPortTimer = setTimeout(() => { state.emit('listAllPorts') }, settings.searchTimeout);
      });
    }
  });

  state.on('handshakeToPort', () => {
    if (settings.portIndex < settings.probablePorts.length) {
      Serial.write('initialize-nodejs-arduino\n', (err) => {
        if (err) {
          isDebug ? console.log('Failed to write on Arduino in ' + settings.probablePorts[settings.portIndex] + '.') : '';
          
          if (Serial && Serial.isOpen()) {
            Serial.update({ baudRate: 1200 }, (err) => {
              if (err) {
                isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' failed to update speed upon exit. ' + err) : '';
              } else {
                isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' changed speed upon exit.') : '';
              }

              Serial.close((err) => {
                if (err) {
                  isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' failed to close upon exit. ' + err) : '';
                  return state.emit('openAPort');
                } else {
                  isDebug ? console.log('Arduino in ' + settings.probablePorts[settings.portIndex] + ' properly closed upon exit.') : '';
                  return state.emit('openAPort');
                }
              });
            });
          }
        } else {
          settings.startedHandshake = true;
        }
      });
    }
  });

  state.on('serialResponse', (data) => {
    let response = data.trim();
    if (settings.startedHandshake) {
      if (response === 'acknowledged-nodejs-arduino') {
        isDebug ? console.log('Arduino acknowledged.') : '';
        mainWindow.webContents.send('arduino-state', 'connected');
        setTimeout(() => { Serial.write('\n'); }, settings.searchTimeout * 1);
        setTimeout(() => { Serial.write('\n'); }, settings.searchTimeout * 2);
      } else if (settings.boardType === 'undefined') {
        isDebug ? console.log('Arduino failed.') : '';
        Serial && Serial.isOpen() ? Serial.close() : '';
        state.emit('openAPort');
      }
      settings.startedHandshake = false;
      settings.introduction = true;
    } else if (settings.introduction) {
      if (response === 'nismed-detecting-motion' || response === 'nismed-weather-station' || response === 'nismed-thermal-equilibrium' || response === 'nismed-earthquake-sensing') {
        isDebug ? console.log(`Got ${response}.`) : '';
        mainWindow.webContents.send('arduino-intro', response);
        settings.boardType = response;
      } else {
        isDebug ? console.log('Cannot recognize module.') : '';
      }
      settings.introduction = false;
    } else if (settings.boardType === 'nismed-detecting-motion') {
      mainWindow.webContents.send('motion-data', response);
    } else if (settings.boardType === 'nismed-weather-station') {
      mainWindow.webContents.send('weather-data', response);
    } else if (settings.boardType === 'nismed-thermal-equilibrium') {
      isDebug ? console.log('Sending data to thermal equilibrium module...') : '';
      mainWindow.webContents.send('equilibrium-data', response);
    } else if (settings.boardType === 'nismed-earthquake-sensing') {
      mainWindow.webContents.send('earthquake-data', response);
    }
  });

}

module.exports = serialListeners;