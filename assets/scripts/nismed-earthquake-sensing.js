'use strict';

const electron = require('electron');
const remote = electron.remote;
const dialog = remote.dialog;
const fs = require('fs');
const path = require('path');
const canvasBuffer = require('electron-canvas-to-buffer');

const ipc = electron.ipcRenderer;

// const dayOfWeekText = document.getElementsByClassName('earthquake-day-of-week')[0];
// const dayText = document.getElementsByClassName('earthquake-day')[0];
// const windSpeedText = document.getElementsByClassName('wind-speed')[0];
// const windDirectionText = document.getElementsByClassName('wind-direction')[0];
// const rainFallText = document.getElementsByClassName('rain-fall')[0];
// const temperatureText = document.getElementsByClassName('temp-data')[0];
// const compass = document.getElementsByClassName('earthquake-compass')[0];
// const rainIcon = document.getElementsByClassName('earthquake-rain')[0];

const tremorLatestAccelText = document.getElementsByClassName('tremor-latest-accel')[0];
const tremorLatestTimeText = document.getElementsByClassName('tremor-latest-time')[0];
const tremorLargestAccelText = document.getElementsByClassName('tremor-largest-accel')[0];
const tremorLargestTimeText = document.getElementsByClassName('tremor-largest-time')[0];

const graphsButton = document.getElementsByClassName('earthquake-save-graphs')[0];
const valuesButton = document.getElementsByClassName('earthquake-save-values')[0];
const resetButton = document.getElementsByClassName('earthquake-reset')[0];
const recordButton = document.getElementsByClassName('earthquake-record')[0];

// let windCanvas = document.getElementById('wind-chart');
// let rainCanvas = document.getElementById('rain-chart');
// let tempCanvas = document.getElementById('temp-chart');
// let windCtx = windCanvas.getContext('2d');
// let rainCtx = rainCanvas.getContext('2d');
// let tempCtx = tempCanvas.getContext('2d');
let eAccelCanvas = document.getElementById('earthquake-accel-chart');
let tremorsCanvas = document.getElementById('tremors-chart');
let eAccelCtx = eAccelCanvas.getContext('2d');
let tremorsCtx = tremorsCanvas.getContext('2d');

// document.addEventListener('DOMContentLoaded', (event) => {
    // dayOfWeekText.innerHTML = moment().format('dddd');
    // dayText.innerHTML = moment().format('DD MMMM YYYY');
// });

// let windData = {
//     datasets: [
//         {
//             label: 'Wind Speed',
//             borderWidth: 2,
//             fill: true,
//             borderColor: 'rgba(255,99,132,1)',
//             backgroundColor: 'rgba(255,99,132,0.3)',
//             // pointStyle: 'crossRot',
//             pointRadius: 0,
//             spanGaps: true,
//             data: []
//         }
//     ]
// };

// let tempData = {
//     datasets: [
//         {
//             label: 'Ambient Temperature',
//             borderWidth: 2,
//             fill: true,
//             borderColor: 'rgba(220,16,239,1)',
//             backgroundColor: 'rgba(220,16,239,0.3)',
//             // pointStyle: 'crossRot',
//             pointRadius: 0,
//             spanGaps: true,
//             data: []
//         }
//     ]
// };

// let rainData = {
//     datasets: [
//         {
//             label: 'Rainfall',
//             borderWidth: 2,
//             fill: true,
//             borderColor: 'rgba(255,196,13,1)',
//             backgroundColor: 'rgba(255,196,13,0.3)',
//             // pointStyle: 'crossRot',
//             pointRadius: 0,
//             spanGaps: true,
//             data: []
//         }
//     ]
// };

let eAccelData = {
    datasets: [
        {
            label: 'Acceleration',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(4,99,128,1)',
            backgroundColor: 'rgba(4,99,128,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 2,
            spanGaps: true,
            data: []
        }, 
        {
            label: 'Cutoff',
            borderWidth: 2,
            borderDash: [5, 5],
            fill: false,
            borderColor: 'rgba(120,120,132,1)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let tremorsData = {
    datasets: [
        {
            label: 'Acceleration',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(0,47,47,1)',
            backgroundColor: 'rgba(0,47,47,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 2,
            lineTension: 0,
            spanGaps: true,
            data: [0]
        }
    ]
};

// let chartOptions = {
//     animationSteps: 15,
//     responsive: true,
//     maintainAspectRatio: false,
//     scales: {
//         xAxes: [{
//             // type: 'time',
//             position: 'bottom',
//             type: 'linear',
//             // type: 'ticks',
//             // position: 'left',
//             // beginAtZero: 'true'
//         }],
//         yAxes: [{
//             scaleLabel: {
//                 display: true,
//                 labelString: 'wow'
//             }
//         }]
//     },
//     legend: {
//         display: false
//     }
// }

let chartOptions = {
    animationSteps: 15,
    responsive: true,
    maintainAspectRatio: false,
    scaleOverride: true,
    scaleSteps: 20,
    scaleStepWidth: 10,
    scaleStartValue: 0,
    scales: {
        xAxes: [{
            display: false
        }],
        yAxes: [{
            scaleLabel: {
                display: true
            }
        }]
    },
    legend: {
        display: false
    }
}

let chartOptions2 = {
    animationSteps: 15,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            scaleLabel: {
                display: true
            }
        }]
    },
    legend: {
        display: false
    }
}

// let windOptions = JSON.parse(JSON.stringify(chartOptions));
// let rainOptions = JSON.parse(JSON.stringify(chartOptions));
// let tempOptions = JSON.parse(JSON.stringify(chartOptions));

let eAccelOptions = JSON.parse(JSON.stringify(chartOptions));
let tremorsOptions = JSON.parse(JSON.stringify(chartOptions2));

// windOptions.scales.yAxes[0].scaleLabel.labelString = 'Wind Speed (km/h)';
// rainOptions.scales.yAxes[0].scaleLabel.labelString = 'Rainfall (mm/h)';
// tempOptions.scales.yAxes[0].scaleLabel.labelString = 'Temperature (°C)';

eAccelOptions.scales.yAxes[0].scaleLabel.labelString = 'Acceleration (mg)';
tremorsOptions.scales.yAxes[0].scaleLabel.labelString = 'Acceleration (mg)';

// let windChart = new Chart.Line(windCtx, {data: windData, options: windOptions});
// let rainChart = new Chart.Line(rainCtx, {data: rainData, options: rainOptions});
// let tempChart = new Chart.Line(tempCtx, {data: tempData, options: tempOptions});

let eAccelChart = new Chart.Line(eAccelCtx, {data: eAccelData, options: eAccelOptions});
let tremorsChart = new Chart.Line(tremorsCtx, {data: tremorsData, options: tremorsOptions});

// let directionConversion = {
//     N: 'North',
//     NE: 'Northeast',
//     E: 'East',
//     SE: 'Southeast',
//     S: 'South',
//     SW: 'Southwest',
//     W: 'West',
//     NW: 'Northwest'
// };

// let directionClasses = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
// let iconClasses = ['sun-o', 'moon-o', 'tint'];
// let directionData = [{x: moment(), y: 'N'}];

let isRecording = false;
let maskValue = 0;
let largestTremor = 0;

ipc.on('earthquake-data', (event, value) => {
    let dataArray = value.split(' ');
    let accelMag = Math.sqrt(Math.pow(dataArray[0], 2) + Math.pow(dataArray[1], 2), Math.pow(dataArray[2], 2));
    // console.log(dataArray);
    if (isRecording) {
        if (eAccelData.datasets[0].data.length > 500) {
            eAccelData.datasets[0].data.shift();
            eAccelData.datasets[1].data.shift();
            eAccelData.labels.shift();
        }

        if (maskValue === 0) {
            maskValue = accelMag * 1.5;
        } else {
            maskValue = 2.1 * ((accelMag * maskValue) / (accelMag + maskValue));
        }

        if (accelMag > maskValue) {
            tremorsData.datasets[0].data.push(accelMag);
            tremorsData.labels.push(moment().format('MMM d h:mm:ss a'));

            tremorLatestAccelText.innerHTML = `${accelMag.toFixed(4)} mg`;
            tremorLatestTimeText.innerHTML = moment().format('MMM d h:mm:ss a');
            
            if (accelMag > largestTremor) {
                tremorLargestAccelText.innerHTML = `${accelMag.toFixed(4)} mg`;
                tremorLargestTimeText.innerHTML = moment().format('MMM d h:mm:ss a');
                largestTremor = accelMag;
            }
        }

        eAccelData.datasets[0].data.push(accelMag);
        eAccelData.datasets[1].data.push(maskValue);
        eAccelData.labels.push(moment().format('MMM d h:mm:ss a'));
        eAccelChart.update();
        tremorsChart.update();
    }

    // if (dataArray[0] === 'Pluviometer') {
    //     rainData.datasets[0].data.push({x: moment(), y: dataArray[1]});
    //     rainFallText.innerHTML = `${dataArray[1]} mm/h`;
    //     iconClasses.map(x => rainIcon.classList.remove(`fa-${x}`));
    //     if (parseFloat(dataArray[1]) !== 0) {
    //         rainIcon.classList.add(`fa-tint`);
    //     } else if (parseFloat(moment().format("H")) >= 6 && parseFloat(moment().format("H")) < 18) {
    //         rainIcon.classList.add(`fa-sun-o`);
    //     } else {
    //         rainIcon.classList.add(`fa-moon-o`);
    //     }
    // } else if (dataArray[0] === 'Anemometer') {
    //     windData.datasets[0].data.push({x: moment(), y: dataArray[1]});
    //     windSpeedText.innerHTML = `${dataArray[1]} km/h`;
    // } else if (dataArray[0] === 'Thermometer') {
    //     tempData.datasets[0].data.push({x: moment(), y: dataArray[1]});
    //     temperatureText.innerHTML = `${dataArray[1]} °C`;
    // } else if (dataArray[0] === 'WindVane') {
    //     directionData.push({x: moment(), y: dataArray[1]});
    //     windDirectionText.innerHTML = `${directionConversion[dataArray[1]]}`;
    //     directionClasses.map(x => compass.classList.remove(`compass-${x}`));
    //     compass.classList.add(`compass-${dataArray[1]}`);
    // }

    // rainChart.update();
    // windChart.update();
    // tempChart.update();
});

graphsButton.addEventListener('click', (event) => {
    // let wind = canvasBuffer(windCanvas, 'image/png');
    // let temp = canvasBuffer(tempCanvas, 'image/png');
    // let rain = canvasBuffer(rainCanvas, 'image/png');

    let eAccel = canvasBuffer(eAccelCanvas, 'image/png');
    let tremors = canvasBuffer(tremorsCanvas, 'image/png');

    dialog.showOpenDialog({properties: ['openDirectory', 'createDirectory']}, (filePaths) => {
        if (filePaths === undefined) return;

        fs.writeFile(path.join(filePaths[0], 'seismogram.png'), eAccel, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'detectedTremors.png'), tremors, (err) => { err ? console.log(err) : null });
    });

    // dialog.showOpenDialog({properties: ['openDirectory', 'createDirectory']}, (filePaths) => {
    //     if (filePaths === undefined) return;

    //     fs.writeFile(path.join(filePaths[0], 'windSpeed.png'), wind, (err) => { err ? console.log(err) : null });
    //     fs.writeFile(path.join(filePaths[0], 'temperature.png'), temp, (err) => { err ? console.log(err) : null });
    //     fs.writeFile(path.join(filePaths[0], 'rainFall.png'), rain, (err) => { err ? console.log(err) : null });
    // });
});

valuesButton.addEventListener('click', (event) => {
    // let wind = windData.datasets[0].data;
    // let temp = tempData.datasets[0].data;
    // let rain = rainData.datasets[0].data;
    // let dir = directionData;

    let eAccelX = eAccelData.labels;
    let eAccelY = eAccelData.datasets[0].data;
    let tremorsX = tremorsData.labels;
    let tremorsY = tremorsData.datasets[0].data;

    // let toCSV = '';
    // toCSV += 'time1,windspeed-kmph,time2,winddir,time3,temperature-celsius,time4,rainfall-mmph\n';

    let toCSV = '';
    toCSV += 'time1,seismogram-mg,time2,tremors-g\n';

    // console.log(wind, temp, rain, dir);
    // wind.map((k, i) => {
    //     toCSV += `${k.x}, ${k.y}, ${dir[i] ? dir[i].x : ''}, ${dir[i] ? dir[i].y : ''}, ${temp[i] ? temp[i].x : ''}, ${temp[i] ? temp[i].y : ''}, ${rain[i] ? rain[i].x : ''}, ${rain[i] ? rain[i].y : ''}\n`;
    //     // toCSV += `${k.x},${k.y},${dir[i].x},${dir[i].y},${rain[i].x},${rain[i].y}\n`;
    // });

    eAccelY.map((k, i) => {
        toCSV += `${eAccelX[i] ? eAccelX[i] : ''}, ${k}, ${tremorsX[i] ? tremorsX[i] : ''}, ${tremorsY[i] ? tremorsY[i] : ''}\n`;
    })

    dialog.showSaveDialog({defaultPath: 'earthquakeSensing.csv'}, (fileName) => {
    // dialog.showSaveDialog({defaultPath: 'simpleearthquakeStation.csv'}, (fileName) => {
        if (fileName === undefined) return;
        
        fs.writeFile(fileName, toCSV, (err) => { err ? console.log(err) : null });
    });
});

recordButton.addEventListener('click', (event) => {
    if (isRecording) {
        recordButton.classList.remove('stop');
        recordButton.classList.add('start');
        recordButton.innerHTML = '<i data-motion="record" class="fa fa-play fa-fw" aria-hidden="true"></i> Start Recording';
        
        graphsButton.classList.remove('hide');
        valuesButton.classList.remove('hide');
    } else {
        recordButton.classList.remove('start');
        recordButton.classList.add('stop');
        recordButton.innerHTML = '<i data-motion="record" class="fa fa-stop fa-fw" aria-hidden="true"></i> Stop Recording';
        
        graphsButton.classList.add('hide');
        valuesButton.classList.add('hide');
    }
    isRecording = ~isRecording;
});

resetButton.addEventListener('click', (event) => {
    eAccelData.datasets[0].data = [];
    eAccelData.datasets[1].data = [];
    tremorsData.datasets[0].data = [];
    eAccelData.labels = [];
    tremorsData.labels = [];

    eAccelChart.update();
    tremorsChart.update();
});