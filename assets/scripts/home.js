'use strict';

const electron = require('electron');

const ipc = electron.ipcRenderer;

const promptContents = document.getElementsByClassName('home-section')[0];

let stillWaiting = false;

let homePrompts = {
  firstGreet: '<h1>Hi there.</h1>',
  welcome: '<h3>Welcome to Listeneer.</h3>',
  searchArduino: '<h3>We\'re finding your Arduino. Please connect it to your PC. <i class="fa fa-circle-o-notch fa-spin fa-lf fa-fw arduino-buffering"></i></h3>',
  boardConfirmed: '<h3>Arduino connected.</h3>',
  recognize: `<h3>Loading <b>%currentModule</b>. Please wait.</h3>`,
  searchTimeout: '<h4>If the Arduino has been connected but not recognized, please restart this program.</h4>'
};

let modules = {
  "nismed-detecting-motion": 'Physics: Detecting Motion',
  "nismed-weather-station": 'Earth/Environmental Science: Simple Weather Station',
  "nismed-thermal-equilibrium" : 'Physics: Thermal Equilibrium',
  "nismed-earthquake-sensing": 'Physics: Earthquake Sensing'
};

document.addEventListener('DOMContentLoaded', (event) => {
  setTimeout(() => { ipc.send('initReady'); }, 0);
  setTimeout(() => { promptContents.insertAdjacentHTML('beforeend', homePrompts.firstGreet); }, 100);
  setTimeout(() => { promptContents.insertAdjacentHTML('beforeend', homePrompts.welcome); }, 600);
  setTimeout(() => { promptContents.insertAdjacentHTML('beforeend', homePrompts.searchArduino); stillWaiting = true; }, 1200);
  setTimeout(() => { stillWaiting ? promptContents.insertAdjacentHTML('beforeend', homePrompts.searchTimeout) : null; }, 7000);
});

ipc.on('arduino-state', (event, status) => {
  if (status === 'connected') {
    stillWaiting = false;
    promptContents.insertAdjacentHTML('beforeend', homePrompts.boardConfirmed);
  } else if (status === 'disconnected') {
    const sections = document.getElementsByClassName('is-shown');
    Array.prototype.forEach.call(sections, (section) => {
      section.classList.remove('is-shown');
    });
    document.getElementsByClassName('home-section')[0].classList.add('is-shown');
    stillWaiting = true;
    promptContents.insertAdjacentHTML('beforeend', homePrompts.searchArduino); 
    setTimeout(() => { stillWaiting ? promptContents.insertAdjacentHTML('beforeend', homePrompts.searchTimeout) : null; }, 6000);
  }
});

ipc.on('arduino-intro', (event, currentModule) => {
  promptContents.insertAdjacentHTML('beforeend', homePrompts.recognize.replace('%currentModule', modules[currentModule]));
  setTimeout(() => {
    const sections = document.getElementsByClassName('is-shown');
    Array.prototype.forEach.call(sections, (section) => {
      section.classList.remove('is-shown');
    });
	  document.getElementsByClassName(currentModule + '-section')[0].classList.add('is-shown');
  }, 2000);
});