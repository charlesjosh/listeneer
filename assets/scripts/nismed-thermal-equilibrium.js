'use strict';

const electron = require('electron');
const remote = electron.remote;
const dialog = remote.dialog;
const fs = require('fs');
const path = require('path');
const canvasBuffer = require('electron-canvas-to-buffer');

const ipc = electron.ipcRenderer;

const dayOfWeekText = document.getElementsByClassName('thermal-day-of-week')[0];
const dayText = document.getElementsByClassName('thermal-day')[0];
const windSpeedText = document.getElementsByClassName('wind-speed')[0];
const windDirectionText = document.getElementsByClassName('wind-direction')[0];
const rainFallText = document.getElementsByClassName('rain-fall')[0];
const temperatureText1 = document.getElementsByClassName('temp-data1')[0];
const temperatureText2 = document.getElementsByClassName('temp-data2')[0];
const compass = document.getElementsByClassName('thermal-compass')[0];
const rainIcon = document.getElementsByClassName('thermal-rain')[0];

const startButton = document.getElementsByClassName('thermal-save-start')[0];
const graphsButton = document.getElementsByClassName('thermal-save-graphs')[0];
const valuesButton = document.getElementsByClassName('thermal-save-values')[0];

let tempCanvas = document.getElementById('thermal-chart');
let tempDiffCanvas = document.getElementById('thermal-difference-chart');

let tempCtx = tempCanvas.getContext('2d');
let tempDiffCtx = tempDiffCanvas.getContext('2d');

var run_Reading = 0;

document.addEventListener('DOMContentLoaded', (event) => {
    dayOfWeekText.innerHTML = moment().format('dddd');
});

let tempData = {
    datasets: [
        {
            label: 'Probe #1',
            borderWidth: 2,
            fill: false,
            borderColor: 'rgba(220,16,239,1)',
            backgroundColor: 'rgba(220,16,239,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        },
        {
            label: 'Probe #2',
            borderWidth: 2,
            fill: false,
            borderColor: 'rgba(0,255,255,1)',
            backgroundColor: 'rgba(220,16,239,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let tempDiffData = {
    datasets: [
        {
            label: 'Difference',
            borderWidth: 2,
            fill: false,
            borderColor: 'rgba(220,16,239,1)',
            backgroundColor: 'rgba(220,16,239,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
}

let chartOptions = {
    animationSteps: 15,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
            type: 'time',
            position: 'bottom'
        }],
        yAxes: [{
            scaleLabel: {
                display: true,
                labelString: 'wow'
            }
        }]
    },
    legend: {
        display: true
    }
}

let tempOptions = JSON.parse(JSON.stringify(chartOptions));

tempOptions.scales.yAxes[0].scaleLabel.labelString = 'Temperature (°C)';

// let windChart = new Chart.Line(windCtx, {data: windData, options: windOptions});
// let rainChart = new Chart.Line(rainCtx, {data: rainData, options: rainOptions});
let tempChart = new Chart.Line(tempCtx, {type: 'line', data: tempData, options: tempOptions});
let tempDiffChart = new Chart.Line(tempDiffCtx, {type: 'line', data: tempDiffData, options: tempOptions});


ipc.on('equilibrium-data', (event, value) => {
    let dataArray = value.split(',');

    if(run_Reading === 1){
        let current_moment = moment();
        tempData.datasets[0].data.push({x: current_moment, y: dataArray[0]});

        tempData.datasets[1].data.push({x: current_moment, y: dataArray[1]});

        tempDiffData.datasets[0].data.push({x: current_moment, y: dataArray[1] - dataArray[0]});

        tempChart.update();
        tempDiffChart.update();
        console.log('Updating chart');
    }

    // Continuously update sidebar values.
    temperatureText1.innerHTML = `${dataArray[0]} °C`;
    temperatureText2.innerHTML = `${dataArray[1]} °C`;
});

graphsButton.addEventListener('click', (event) => {
    let temp = canvasBuffer(tempCanvas, 'image/png');
    let temp2 = canvasBuffer(tempDiffCanvas, 'image/png');

    dialog.showOpenDialog({properties: ['openDirectory', 'createDirectory']}, (filePaths) => {
        if (filePaths === undefined) return;

        fs.writeFile(path.join(filePaths[0], 'temperature.png'), temp, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'temperature_difference.png'), temp2, (err) => { err ? console.log(err) : null });
    });
});

valuesButton.addEventListener('click', (event) => {
    let temp = tempData.datasets[0].data;
    let temp2 = tempData.datasets[1].data;
    let tempDiff = tempDiffData.datasets[0].data;

    let toCSV = '';
    toCSV += 'Time of day, Probe #1 (°C), Probe #2 (°C), Difference (°C)\n';
    console.log(temp, temp2);
    for (var i = 0; i < temp.length; i++) {
        toCSV += temp[i].x.format('h:mm:ss a') + ',' + temp[i].y +  ',' + temp2[i].y +  ',' + tempDiff[i].y + '\n';
    };

    dialog.showSaveDialog({defaultPath: 'thermalEquilibrium.csv'}, (fileName) => {
        if (fileName === undefined) return;
        
        fs.writeFile(fileName, toCSV, (err) => { err ? console.log(err) : null });
    });
});

startButton.addEventListener('click', (event) => {
    // Allow to hold the data after clicking stop, reset data only during restart.
    if(run_Reading === 0) {
        tempData.datasets[0].data.length = 0;
        tempData.datasets[1].data.length = 0;
        tempDiffData.datasets[0].data.length = 0;

        temperatureText1.innerHTML = 'Waiting';
        temperatureText2.innerHTML = 'Waiting';

        tempChart.update();
        tempDiffChart.update();

        startButton.classList.remove('start');
        startButton.classList.add('stop');
        startButton.innerHTML = '<i data-motion="record" class="fa fa-stop fa-fw" aria-hidden="true"></i> Stop Recording';

        graphsButton.classList.add('hide');
        valuesButton.classList.add('hide');
        run_Reading = 1;
    }
    else {
        run_Reading = 0;
        startButton.classList.remove('stop');
        startButton.classList.add('start');
        startButton.innerHTML = '<i data-motion="record" class="fa fa-play fa-fw" aria-hidden="true"></i> Start Recording';

        graphsButton.classList.remove('hide');
        valuesButton.classList.remove('hide');
    }
});