'use strict';

const electron = require('electron');
const remote = electron.remote;
const dialog = remote.dialog;
const fs = require('fs');
const path = require('path');
const canvasBuffer = require('electron-canvas-to-buffer');

const ipc = electron.ipcRenderer;

const dayOfWeekText = document.getElementsByClassName('weather-day-of-week')[0];
const dayText = document.getElementsByClassName('weather-day')[0];
const windSpeedText = document.getElementsByClassName('wind-speed')[0];
const windDirectionText = document.getElementsByClassName('wind-direction')[0];
const rainFallText = document.getElementsByClassName('rain-fall')[0];
const temperatureText = document.getElementsByClassName('temp-data')[0];
const compass = document.getElementsByClassName('weather-compass')[0];
const rainIcon = document.getElementsByClassName('weather-rain')[0];

const graphsButton = document.getElementsByClassName('weather-save-graphs')[0];
const valuesButton = document.getElementsByClassName('weather-save-values')[0];

let windCanvas = document.getElementById('wind-chart');
let rainCanvas = document.getElementById('rain-chart');
let tempCanvas = document.getElementById('temp-chart');
let windCtx = windCanvas.getContext('2d');
let rainCtx = rainCanvas.getContext('2d');
let tempCtx = tempCanvas.getContext('2d');

document.addEventListener('DOMContentLoaded', (event) => {
    dayOfWeekText.innerHTML = moment().format('dddd');
    dayText.innerHTML = moment().format('DD MMMM YYYY');
});

let windData = {
    datasets: [
        {
            label: 'Wind Speed',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(255,99,132,1)',
            backgroundColor: 'rgba(255,99,132,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let tempData = {
    datasets: [
        {
            label: 'Ambient Temperature',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(220,16,239,1)',
            backgroundColor: 'rgba(220,16,239,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let rainData = {
    datasets: [
        {
            label: 'Rainfall',
            borderWidth: 2,
            fill: true,
            borderColor: 'rgba(255,196,13,1)',
            backgroundColor: 'rgba(255,196,13,0.3)',
            // pointStyle: 'crossRot',
            pointRadius: 0,
            spanGaps: true,
            data: []
        }
    ]
};

let chartOptions = {
    animationSteps: 15,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
            type: 'time',
            position: 'bottom'
        }],
        yAxes: [{
            scaleLabel: {
                display: true,
                labelString: 'wow'
            }
        }]
    },
    legend: {
        display: false
    }
}

let windOptions = JSON.parse(JSON.stringify(chartOptions));
let rainOptions = JSON.parse(JSON.stringify(chartOptions));
let tempOptions = JSON.parse(JSON.stringify(chartOptions));

windOptions.scales.yAxes[0].scaleLabel.labelString = 'Wind Speed (km/h)';
rainOptions.scales.yAxes[0].scaleLabel.labelString = 'Rainfall (mm/h)';
tempOptions.scales.yAxes[0].scaleLabel.labelString = 'Temperature (°C)';

let windChart = new Chart.Line(windCtx, {data: windData, options: windOptions});
let rainChart = new Chart.Line(rainCtx, {data: rainData, options: rainOptions});
let tempChart = new Chart.Line(tempCtx, {data: tempData, options: tempOptions});

let directionConversion = {
    N: 'North',
    NE: 'Northeast',
    E: 'East',
    SE: 'Southeast',
    S: 'South',
    SW: 'Southwest',
    W: 'West',
    NW: 'Northwest'
};

let directionClasses = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
let iconClasses = ['sun-o', 'moon-o', 'tint'];
let directionData = [{x: moment(), y: 'N'}];

ipc.on('weather-data', (event, value) => {
    let dataArray = value.split(' ');

    if (dataArray[0] === 'Pluviometer') {
        rainData.datasets[0].data.push({x: moment(), y: dataArray[1]});
        rainFallText.innerHTML = `${dataArray[1]} mm/h`;
        iconClasses.map(x => rainIcon.classList.remove(`fa-${x}`));
        if (parseFloat(dataArray[1]) !== 0) {
            rainIcon.classList.add(`fa-tint`);
        } else if (parseFloat(moment().format("H")) >= 6 && parseFloat(moment().format("H")) < 18) {
            rainIcon.classList.add(`fa-sun-o`);
        } else {
            rainIcon.classList.add(`fa-moon-o`);
        }
    } else if (dataArray[0] === 'Anemometer') {
        windData.datasets[0].data.push({x: moment(), y: dataArray[1]});
        windSpeedText.innerHTML = `${dataArray[1]} km/h`;
    } else if (dataArray[0] === 'Thermometer') {
        tempData.datasets[0].data.push({x: moment(), y: dataArray[1]});
        temperatureText.innerHTML = `${dataArray[1]} °C`;
    } else if (dataArray[0] === 'WindVane') {
        directionData.push({x: moment(), y: dataArray[1]});
        windDirectionText.innerHTML = `${directionConversion[dataArray[1]]}`;
        directionClasses.map(x => compass.classList.remove(`compass-${x}`));
        compass.classList.add(`compass-${dataArray[1]}`);
    }

    rainChart.update();
    windChart.update();
    tempChart.update();
});

graphsButton.addEventListener('click', (event) => {
    let wind = canvasBuffer(windCanvas, 'image/png');
    let temp = canvasBuffer(tempCanvas, 'image/png');
    let rain = canvasBuffer(rainCanvas, 'image/png');

    dialog.showOpenDialog({properties: ['openDirectory', 'createDirectory']}, (filePaths) => {
        if (filePaths === undefined) return;

        fs.writeFile(path.join(filePaths[0], 'windSpeed.png'), wind, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'temperature.png'), temp, (err) => { err ? console.log(err) : null });
        fs.writeFile(path.join(filePaths[0], 'rainFall.png'), rain, (err) => { err ? console.log(err) : null });
    });
});

valuesButton.addEventListener('click', (event) => {
    let wind = windData.datasets[0].data;
    let temp = tempData.datasets[0].data;
    let rain = rainData.datasets[0].data;
    let dir = directionData;
    let toCSV = '';
    toCSV += 'time1,windspeed-kmph,time2,winddir,time3,temperature-celsius,time4,rainfall-mmph\n';
    console.log(wind, temp, rain, dir);
    wind.map((k, i) => {
        toCSV += `${k.x}, ${k.y}, ${dir[i] ? dir[i].x : ''}, ${dir[i] ? dir[i].y : ''}, ${temp[i] ? temp[i].x : ''}, ${temp[i] ? temp[i].y : ''}, ${rain[i] ? rain[i].x : ''}, ${rain[i] ? rain[i].y : ''}\n`;
        // toCSV += `${k.x},${k.y},${dir[i].x},${dir[i].y},${rain[i].x},${rain[i].y}\n`;
    });

    dialog.showSaveDialog({defaultPath: 'simpleWeatherStation.csv'}, (fileName) => {
        if (fileName === undefined) return;
        
        fs.writeFile(fileName, toCSV, (err) => { err ? console.log(err) : null });
    });
});