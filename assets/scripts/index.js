'use strict';

const electron = require('electron');

const ipc = electron.ipcRenderer;

let targetPage = null;

document.body.addEventListener('click', (event) => {
	// if (event.target.dataset.motion) {
	  // handleMotionCall(event.target.dataset.motion);
	// } else if (event.target.dataset.link) {
	// 	handleJumpLink(event.target.dataset.link);
	// } else if (event.target.dataset.trigger) {
	// 	customTriggers(event);
	// } else if (event.target.dataset.conditional) {
	// 	handleConditionalLink(event);
	// }
});

function actualJump (link) {
	let targetPage = null;
	// if (link === 'home') {
	//   appHistory.length = 0;
	//   targetPage = 'home';
	// } else if (link === 'back') {
	//   targetPage = appHistory.splice(-2)[0];
	// } else if (link === 'customer') {
	//   targetPage = link;
	// 	vm.refresh();
	// } else if (link === 'generate') {
	//   targetPage = link;
	// 	receipt.refresh();
	// } else {
	//   targetPage = link;
	// }
  targetPage = link;

	// if (targetPage !== 'saving')
	// 	appHistory.push(targetPage);

	// if (targetPage === 'home')
	// 	document.body.classList.add('inHome');
	// else
	// 	document.body.classList.remove('inHome');

	hideAllSections();
	document.getElementsByClassName(targetPage + '-section')[0].classList.add('is-shown');
}

function hideAllSections () {
  const sections = document.getElementsByClassName('is-shown');
  Array.prototype.forEach.call(sections, (section) => {
    section.classList.remove('is-shown');
  });
}

String.prototype.format = function () {
  var i = 0, args = arguments;
  return this.replace(/{}/g, function () {
    return typeof args[i] != 'undefined' ? args[i++] : '';
  });
};