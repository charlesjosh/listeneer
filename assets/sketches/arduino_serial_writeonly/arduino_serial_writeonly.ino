const int ledPins[6] = {8, 9, 10, 11, 12, 13};
bool ledState = false;
String prompt;

void setup() {
  Serial.begin(9600);
  Serial.println("Hello!");
  for (int i = 0; i < 6; i++)
    pinMode(ledPins[i], OUTPUT);
  while (true) {
    if (Serial.available() > 0) {
      prompt = Serial.readString();
      //Serial.print(prompt);
      if (prompt == "initialize-nodejs-arduino\n") {
        Serial.println("acknowledged-nodejs-arduino");
        Serial.println("nismed-detecting-motion");
        break;
      }
    }
  }
}

void loop() {
  while (Serial.available() == 0);
  Serial.read();
  ledState = !ledState;
  for (int i = 0; i < 6; i++)
    digitalWrite(ledPins[i], ledState);
}
