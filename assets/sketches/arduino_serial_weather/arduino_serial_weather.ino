const int windVanePin = A0;
const int thermometerPin = A1;
const int anemometerPin = 2;
const int pluviometerPin = 3;
const int redLED = 4;
const int greenLED = 5;
const int orangeLED = 6;

const float K_PLUVIO = 0.2794;    // in millimeters per hour
const float K_ANEM = 2.4000;      // in kilometers per hour
//const float K_ANEM = 0.6667;      // in meters per second

int rainTicks = 0;
int windSpeedTicks = 0;
int lastDirection = -1;

bool redState = false;
bool greenState = false;
bool orangeState = false;

char equiv[][3] = {"N","NE","E","SE","S","SW","W","NW"};
float val[] = {39.4,23.15,4.6,9.2,14.4,31.6,47.3,44.4};

unsigned long initTime = 0;
unsigned long pluvioTime = 0;
unsigned long lastAnem = 0;
unsigned long lastPluvio = 0;

String prompt;
char buff[30];

void setup () {
  pinMode(orangeLED, OUTPUT);
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(pluviometerPin, INPUT_PULLUP);
  pinMode(anemometerPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pluviometerPin), waterTick, FALLING);
  attachInterrupt(digitalPinToInterrupt(anemometerPin), halfSpinTick, FALLING);

  Serial.begin(9600);
  Serial.println("Hello!");

  while (true) {
    if (Serial.available() > 0) {
      prompt = Serial.readString();
      //Serial.print(prompt);
      if (prompt == "initialize-nodejs-arduino\n") {
        Serial.println("acknowledged-nodejs-arduino");
        Serial.println("nismed-weather-station");
        break;
      }
    }
  }

   initTime = pluvioTime =  millis();
}

void loop () {

  if (millis() > initTime + 1000) {
    noInterrupts();
      Serial.print("Pluviometer ");
      Serial.println(getRainFall());
      Serial.print("Anemometer ");
      Serial.println(getWindSpeed());
      Serial.print("Thermometer ");
      Serial.println(getTemp());
    interrupts();

    windSpeedTicks = 0;
    initTime = millis();
  }

  if (millis() > pluvioTime + 3600000) {
    rainTicks = 0;
    pluvioTime = millis();
  }

  int ndir = getDir();
  if (ndir != lastDirection) {
    lastDirection = ndir;
    Serial.print("WindVane ");
    Serial.println(equiv[ndir]);
    redState = !redState;
    digitalWrite(redLED, redState);
  }

  delay(500);
}

int getDir () {
  float sensorData = ((analogRead(windVanePin) / 2) / 10.0);
  float err = 1000;
  float nerr = 0;
  int ndir = -1;
  
//  Serial.println(sensorData);

  for (int i = 0; i < 8; i++) {
    nerr = fabs(sensorData - val[i]);
    if (nerr < err) {
      ndir = i;
      err = nerr;
    }
  }

  return ndir;
}

float getRainFall () {
  return rainTicks * K_PLUVIO;
}

float getWindSpeed () {
  return (windSpeedTicks / 2.00) * K_ANEM;
}

float getTemp () {
  return (((analogRead(thermometerPin) * 5.0) / 1024.0) - 0.5) * 100.0;
}

void waterTick () {
  if (millis() > lastPluvio + 15) {
    rainTicks++;
    orangeState = !orangeState;
    digitalWrite(orangeLED, orangeState);
    lastPluvio = millis();
  }
}

void halfSpinTick() {
  if(millis() > lastAnem + 15) {
    windSpeedTicks++;
    greenState = !greenState;
    digitalWrite(greenLED, greenState);
    lastAnem = millis();
  }
}



