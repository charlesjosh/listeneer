/*
 * Calibration for the ADXL362.
 * Aside from wiring to pin 10 and to 3v3, connect an LM35 to A0.
 * Make sure not to reverse the pins.
 * 
 * Assumes ground position, for range 8g.
 */
#include <SPI.h>
#include <ADXL362.h>
#include <PreciseLM35.h>

const int pinLM35 = A0;
const float measuredVsReadArefRatio = 1.001799; // enter here your ratio between measured and expected AREF
PreciseLM35 lm35(pinLM35, DEFAULT, measuredVsReadArefRatio);

ADXL362 accel;
int16_t XValue, YValue, ZValue, Temperature;
float TemperatureLM35;
float tempOffset;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  accel.begin(10);      // Set CS pin to 10.
  accel.setRange(0);
  accel.beginMeasure();
}

void loop() {
  // put your main code here, to run repeatedly:
  accel.readXYZTData(XValue, YValue, ZValue, Temperature);
  TemperatureLM35 = lm35.readCelsius();
  tempOffset = (float) Temperature - ((float) TemperatureLM35 / 0.065);
  Serial.println("X: " + String(XValue) + " | Y: " + String(YValue) + " | Z: " + String(ZValue) + " | Temp: " + String(Temperature) + " | LM35: " + String(TemperatureLM35) + " | Temp Offset: " + String(tempOffset));
  delay(100);
}
