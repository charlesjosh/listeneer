#include <OneWire.h>

#define ONEWIRE_PIN1 8
#define ONEWIRE_PIN2 9

OneWire ds1(ONEWIRE_PIN1);
OneWire ds2(ONEWIRE_PIN2);
char arr[32];

String prompt;
unsigned long curTime = 0;

void setup(void)
{
  Serial.begin(9600);
  while(!Serial);

  Serial.println("Hello!\n");
  while(true)
  {
    if (Serial.available() > 0) 
    {
      prompt = Serial.readString();
      //Serial.print(prompt);
      if (prompt.startsWith("initialize-nodejs-arduino")) {
        Serial.println("acknowledged-nodejs-arduino");
        Serial.println("nismed-thermal-equilibrium");
        break;
      }
    }
  }
  curTime = millis() + 3000;
}

float get_temp(OneWire &ds)
{
  byte i;
  byte data[12];
  byte addr[8];
  float celsius;

  ds.reset_search();
  if (!ds.search(addr))
  {
    Serial.println("Disconnected");
    return 0;
  }

  if (OneWire::crc8(addr, 7) != addr[7])
  { 
    Serial.println("Data Corrupt");
    return 0;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 0);        // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  
  ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++)
    data[i] = ds.read();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  byte cfg = (data[4] & 0x60);
  // at lower res, the low bits are undefined, so let's zero them
  if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
  else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
  else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
  //// default is 12 bit resolution, 750 ms conversion time
  celsius = (float)raw / 16.0;
  return celsius;
}

void loop(void)
{
  if(curTime > millis())
    return;
  
  float a = get_temp(ds1);
  float b = get_temp(ds2);
  
  Serial.print(a);
  Serial.print(",");
  Serial.println(b);
  curTime += 3000;
}
