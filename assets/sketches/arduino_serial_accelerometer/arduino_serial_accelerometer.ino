#include <SPI.h>
#include <ADXL362.h>

const unsigned int CALIB_SAMPLES = 20;

ADXL362 accel;

bool startSensing = false;
int16_t XValue, YValue, ZValue, Temperature;
int16_t XCalib[CALIB_SAMPLES], YCalib[CALIB_SAMPLES], ZCalib[CALIB_SAMPLES];
float XOffset, YOffset, ZOffset;

String prompt;

void setup () {
  memset(XCalib, 0, sizeof(XCalib));
  memset(YCalib, 0, sizeof(YCalib));
  memset(ZCalib, 0, sizeof(ZCalib));
  
  accel.begin(10);      // Set CS pin to 10.
  accel.setRange(1);    // Set to 4g.
  accel.beginMeasure();

  Serial.begin(9600);
  Serial.println("Hello!");

  while (true) {
    if (Serial.available() > 0) {
      prompt = Serial.readString();
      //Serial.print(prompt);
      if (prompt.startsWith("initialize-nodejs-arduino")) {
        Serial.println("acknowledged-nodejs-arduino");
        Serial.println("nismed-earthquake-sensing");
        break;
      }
    }
  }
}

void loop () {
//  if (Serial.available() > 0) {
//    prompt = Serial.readString();
//    if (prompt.startsWith("calibrate")) {
//      calibrateAxes();
//    } else if (prompt.startsWith("start")) {
//      startSensing = true;
//    } else if (prompt.startsWith("stop")) {
//      startSensing = false;
//    } 
//  } else if (startSensing) {
    accel.readXYZTData(XValue, YValue, ZValue, Temperature);
//    double accelMagnitude = sqrt(sq(XValue) + sq(YValue) + sq(ZValue));
    Serial.println(String(XValue) + " " + String(YValue) + " " + String(ZValue) + " ");
//    Serial.println(String(accelMagnitude));
//  }
  delay(100);
}

void calibrateAxes () {
  // Sample every 0.01s.
  XOffset = 0.0;
  YOffset = 0.0;
  ZOffset = 0.0;
  
  for (int i = 0; i < CALIB_SAMPLES; i++) {
    accel.readXYZTData(XCalib[i], YCalib[i], ZCalib[i], Temperature);
    delay(10);
  }

  // Take the sum.
  for (int i = 0; i < CALIB_SAMPLES; i++) {
    XOffset += XCalib[i];
    YOffset += YCalib[i];
    ZOffset += ZCalib[i];
  }

  // Take the mean.
  XOffset /= CALIB_SAMPLES;
  YOffset /= CALIB_SAMPLES;
  ZOffset /= CALIB_SAMPLES;

  // Print.
  Serial.println("C: " + String(XOffset) + " " + String(YOffset) + " " + String(ZOffset));
}

