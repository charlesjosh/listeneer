const int TRIG_PIN = 8;
const int ECHO_PIN = 9;

const unsigned int samplingInterval = 30;
const unsigned int distanceSamples = 10;
const unsigned int MAX_DIST = 23200;

unsigned long initTime = 0;

String prompt;

float distances[2][3];
float velocities[2][2];
float acceleration[2];

void setup () {
  memset(distances, 0, sizeof(distances));
  memset(velocities, 0, sizeof(velocities));
  memset(acceleration, 0, sizeof(acceleration));

  Serial.begin(9600);
  Serial.println("Hello!");

  pinMode(TRIG_PIN, OUTPUT);
  digitalWrite(TRIG_PIN, LOW);
  
  while (true) {
    if (Serial.available() > 0) {
      prompt = Serial.readString();
      //Serial.print(prompt);
      if (prompt == "initialize-nodejs-arduino\n") {
        Serial.println("acknowledged-nodejs-arduino");
        Serial.println("nismed-weather-station");
        break;
      }
    }
  }

  initTime = millis();

}

void loop() {
  // Grab data.
  distances[0][2] = millis();
  distances[1][2] = getDistance();

  // Compute values.
  velocities[0][1] = distances[0][1] + ((distances[0][2] - distances[0][1]) / 2);
  velocities[1][1] = (distances[1][2] - distances[1][1]) / ((distances[0][2] - distances[0][1]) / 1000);

  acceleration[0] = velocities[0][0] + ((velocities[0][1] - velocities[0][0]) / 2);
  acceleration[1] = (velocities[1][1] - velocities[1][0]) / ((velocities[0][1] - velocities[0][0]) / 1000);
  
  // Send data.
  Serial.print(distances[0][2]);
  Serial.print(" ");
  Serial.print(distances[1][2]);
  Serial.print(" ");
  Serial.print(velocities[0][1]);
  Serial.print(" ");
  Serial.print(velocities[1][1]);
  Serial.print(" ");
  Serial.print(acceleration[0]);
  Serial.print(" ");
  Serial.println(acceleration[1]);

  // Move data.
  distances[0][0] = distances[0][1];
  distances[0][1] = distances[0][2];
  distances[1][0] = distances[1][1];
  distances[1][1] = distances[1][2];
  
  velocities[0][0] = velocities[0][1];
  velocities[1][0] = velocities[1][1];
 }

float getDistance () {
  float summation = 0;
  float rawDistance = 0;
  float count = 0;
  for (int i = 0; i < distanceSamples; i++) {
    rawDistance = getDistanceRaw();
    if (rawDistance != -1) {
      summation += rawDistance;
      count++;
    }
  }
  return summation/count;
}

float getDistanceRaw () {
  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float cm;

  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  while (digitalRead(ECHO_PIN) == 0);

  t1 = micros();
  while (digitalRead(ECHO_PIN) == 1);
  t2 = micros();
  pulse_width = t2 - t1;

  cm = pulse_width / 58.0;

  delay(samplingInterval);

  return (pulse_width > MAX_DIST) ? -1.00 : cm;
}
