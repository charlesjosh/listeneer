const int pushButton = 7;

void setup() {
  Serial.begin(9600);
  Serial.println("Hello!");
  pinMode(pushButton, INPUT);
}

void loop() {
  int buttonState = digitalRead(pushButton);
  if (buttonState == HIGH) {
    Serial.write(1);
  } else {
    Serial.write(0);
  }

  delay(100);
}
