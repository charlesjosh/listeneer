'use strict';

/*
  Links:
    https://nodejs.org/api/events.html
    https://www.npmjs.com/package/serialport

  Recompile serialport on directory with:
    node-pre-gyp rebuild --target=1.4.1 --arch=x64 -dist-url=https://atom.io/download/atom-shell
  Package it with:
    electron-packager . listeneer --platform=win32 --arch=ia32 --icon=headphone.ico
  
  Try changing arch when packaging Electron with 32-bit libraries.
 */

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;
const dialog = electron.dialog;

const EventEmitter = require('events');
const path = require('path');
const url = require('url');

const serialListeners = require('./assets/scripts/serialListeners');
const interprocessListeners = require('./assets/scripts/interprocessListeners');

const isDebug = true;
let settings = {
  searchTimeout: 1000,
  startedHandshake: false,
  introduction: false,
  boardType: 'undefined',
  probablePorts: [],
  portIndex: 0
};

class FSM extends EventEmitter {};
const state = new FSM();

let mainWindow;

function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 600});

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'assets/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.maximize();

  // mainWindow.setMenu(null);
  isDebug ? null : mainWindow.setMenu(null);

  isDebug ? mainWindow.webContents.openDevTools() : null;

  mainWindow.on('closed', function () {
    mainWindow = null;
  });

  serialListeners(isDebug, settings, ipc, state, mainWindow);
  interprocessListeners(isDebug, settings, ipc, state, mainWindow);
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  state.emit('exitScript', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});