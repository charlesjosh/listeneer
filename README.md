# Listeneer
## A Laboratory Module Supplement

**Part of UP NISMED - Bayanihan Cre@tive Labs K-12 Science Laboratory Modules**

### Introduction
Designed as a helper program for UP NISMED – Bayanihan Cre@tive Labs K-12 Science Laboratory Modules, Listeener acts as an interface and logger for various sensors connected to an Arduino setup. Listeneer was built with intuitiveness in mind, and is geared towards helping both the students and the educators make better use of data available through electronic means. 

### Supported Modules
Currently, three modules are supported.

- *Detecting Motion* (Physics) - Tracks and logs the distance, velocity, and acceleration of a moving object along one axis.
- *Simple Weather Station* (Earth/Environmental Science) - Simulates a weather station with sensors for ambient temperature, wind speed, wind direction, and rainfall.
- *Thermal Equilibrium* (Physics) - Measures the temperature between two objects/liquids, and logs it over time.

### Vision
Hopefully, Listeneer will be continued to be improved and built upon by multiple educators and students in order to support more modules and sensors.

### More
An in-depth discussion about the project is on the user manual at `Listeneer - User Manual.pdf`.
